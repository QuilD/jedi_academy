from django.apps import AppConfig


class JediConfig(AppConfig):
    name = 'jedi'
    verbose_name = "Академия (джедаи и падаваны)"
