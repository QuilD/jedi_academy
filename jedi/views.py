from django.core.mail import send_mail
from django.shortcuts import redirect, reverse
from django.views.generic import CreateView, ListView

from jedi.forms import PadavanForm
from jedi.models import Jedi, Padavan


def accept_candidate_to_padavan(request, jedi_id, padavan_id, *args, **kwargs):
    # if user hack template -> disable add padavan if count > 3
    try:
        jedi = Jedi.objects.get(pk=jedi_id)
        if jedi.padavan.count() <= 3:
            padavan = Padavan.objects.get(pk=padavan_id)
            padavan.sensei = jedi
            padavan.save()
            message = f"Здравствуйте, юный падаван {{ padavan.name }}."\
                      f"Да да, теперь вы являетесь падаваном мастера {{ jedi.name }}."
            # Send email if configure email service in django settings
            send_mail("Зачисление в падаваны", message,
                      "yoda@mail.cs", [padavan.email], fail_silently=False)
    except Jedi.DoesNotExist:
        #  TODO Add error flash message
        pass
    return redirect(reverse('result_list', kwargs={'jedi_id': jedi_id}))


class PadavanList(ListView):
    template_name = 'jedi/padavan_list.html'
    queryset = Jedi.objects.select_related().all()
    context_object_name = 'jedi_list'


class PadavanView(CreateView):
    form_class = PadavanForm
    template_name = 'jedi/padavan_form.html'

    def post(self, request, *args, **kwargs):
        self.form = PadavanForm(request.POST)
        if self.form.is_valid():
            padavan = self.form.save()
            return redirect(reverse('exam_list',
                                    kwargs={'padavan_id': padavan.id}))
        else:
            return super().get(request, *args, **kwargs)
