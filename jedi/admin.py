from django.contrib import admin

from .models import Jedi, Padavan


@admin.register(Padavan)
class PadavanAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('name', 'age', 'email', 'planet')
    list_filter = ('planet',)


@admin.register(Jedi)
class JediAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('name', 'planet')
    list_filter = ('planet',)
