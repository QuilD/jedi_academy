from django import forms

from jedi.models import Jedi, Padavan
from location.models import Planet


class PadavanForm(forms.ModelForm):
    planet = forms.ModelChoiceField(queryset=Planet.objects.all(),
                                    required=True, empty_label=None,
                                    label="Планета обитания")

    class Meta:
        model = Padavan
        fields = ['name', 'email', 'age', 'planet']


class JediListForm(forms.Form):
    name = forms.ModelChoiceField(queryset=Jedi.objects.all(),
                                  required=False, empty_label="Выберите Джедая",
                                  label="мастер Джедай",
                                  widget=forms.widgets.Select(
                                    attrs={'class': 'jedi_list'}))
