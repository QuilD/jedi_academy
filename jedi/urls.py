from django.conf.urls import url

from jedi.views import PadavanList, PadavanView, accept_candidate_to_padavan

urlpatterns = [
    url(r'register/$', PadavanView.as_view(), name='register'),
    url(r'accept/(?P<jedi_id>[0-9]+)/(?P<padavan_id>[0-9]+)/$',
        accept_candidate_to_padavan, name='accept'),
    url(r'padavan_list/$', PadavanList.as_view(), name='padavan_list'),
]
