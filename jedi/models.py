from django.db import models

from location.models import Planet


class Jedi(models.Model):
    name = models.CharField(max_length=100, verbose_name="Имя")
    planet = models.ForeignKey(Planet, null=True, blank=True,
                               on_delete=models.SET_NULL,
                               verbose_name="Планета обучения")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Джедай"
        verbose_name_plural = "Джедаи"
        ordering = ['name']


class Padavan(models.Model):
    name = models.CharField(max_length=100, verbose_name="Имя")
    age = models.IntegerField(verbose_name="Возраст")
    email = models.EmailField(verbose_name="Электронная почта")
    planet = models.ForeignKey(Planet, null=True, blank=True,
                               on_delete=models.SET_NULL,
                               verbose_name="Планета обитания")
    sensei = models.ForeignKey(Jedi, null=True, blank=True,
                               on_delete=models.SET_NULL,
                               related_name='padavan', verbose_name="Учитель")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Падаван"
        verbose_name_plural = "Падаваны"
        ordering = ['name']
