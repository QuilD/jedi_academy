# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-20 12:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('examination', '0003_auto_20170520_1459'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='padavan',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='answer', to='jedi.Padavan'),
        ),
    ]
