from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect
from django.views.generic import TemplateView

from examination.forms import ExamForm
from examination.models import Answer, Question
from jedi.forms import JediListForm
from jedi.models import Jedi, Padavan


class ExamResultView(TemplateView):
    template_name = 'examination/result_list.html'
    jedi = None

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['jedi_list'] = self.jedi_list
        context['current_jedi'] = self.jedi
        if self.jedi:
            context['candidate_list'] = Padavan.objects.filter(planet=self.jedi.planet, sensei=None)
        return context

    def get(self, request, jedi_id=0, *args, **kwargs):
        self.jedi_list = JediListForm({'name': jedi_id})
        try:
            self.jedi = Jedi.objects.get(pk=jedi_id)
        except Jedi.DoesNotExist:
            pass
        return super().get(request, jedi_id, *args, **kwargs)

    def post(self, request, jedi_id=0, *args, **kwargs):
        return super().get(request, jedi_id, *args, **kwargs)


class ExamFormView(TemplateView):
    template_name = 'examination/exam_form.html'
    exam_form = None

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['exam_form'] = self.exam_form
        return context

    def get(self, request, padavan_id, *args, **kwargs):
        self.exam_form = ExamForm()
        return super().get(request, *args, **kwargs)

    def post(self, request, padavan_id, *args, **kwargs):
        self.exam_form = ExamForm(request.POST)
        if self.exam_form.is_valid():
            result = self.exam_form.cleaned_data.items()
            for key, value in result:
                try:
                    Answer.objects.create(
                        padavan=Padavan.objects.get(pk=int(padavan_id)),
                        question=Question.objects.get(pk=key),
                        answer=value,
                    )
                except ObjectDoesNotExist:
                    pass
            return redirect('/')
        else:
            return super().get(request, *args, **kwargs)
