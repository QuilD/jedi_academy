from django import forms

from examination.models import Question


class ExamForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for uid, question in (Question.objects.all().values_list('uid', 'text')):
            self.fields[str(uid)] = forms.BooleanField(
                                            label=question, required=False)
