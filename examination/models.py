import uuid

from django.db import models

from jedi.models import Padavan


class Question(models.Model):
    uid = models.UUIDField(primary_key=True, default=uuid.uuid4,
                           editable=False)
    text = models.TextField(verbose_name="Содержание вопроса")
    padavan = models.ManyToManyField(Padavan, through='Answer')

    def __str__(self):
        return str(self.uid)

    class Meta:
        verbose_name = "Вопрос для экзамена"
        verbose_name_plural = "Вопросы для экзамена"


class Answer(models.Model):
    padavan = models.ForeignKey(Padavan, related_name='answer')
    question = models.ForeignKey(Question)
    answer = models.BooleanField()

    def __str__(self):
        return self.question.text
