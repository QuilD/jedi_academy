from django.apps import AppConfig


class ExaminationConfig(AppConfig):
    name = 'examination'
    verbose_name = "Экзамены"
