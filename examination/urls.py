from django.conf.urls import url

from examination.views import ExamFormView, ExamResultView


urlpatterns = [
    url(r'exam_list/(?P<padavan_id>[0-9]+)/$', ExamFormView.as_view(),
        name='exam_list'),
    url(r'result_list/(?P<jedi_id>[0-9]+)/$', ExamResultView.as_view(),
        name='result_list'),
    url(r'result_list/$', ExamResultView.as_view(), name='result_list'),
]
