from django.db import models


class Planet(models.Model):
    name = models.CharField(max_length=64, verbose_name="Наименование")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Планета"
        verbose_name_plural = "Планеты"
        ordering = ['name']
