$(document).ready(function(){
  $('select').material_select();

  $('select.jedi_list').change(function(){
    // Redirect to new page with jedi id
    $this = $(this)
    if ($this.val() != "") {
      window.location = "/exam/result_list/" + $this.val() + "/";
    }
  })
});
